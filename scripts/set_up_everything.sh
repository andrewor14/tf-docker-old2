#!/bin/bash

# ========================================================================= #
#  Helper script to set up environment.                                     #
#                                                                           #
#  This must be run on all nodes at the same time.                          #
#  Some useful environment variables:                                       #
#                                                                           #
#    SKIP_BUILD_IMAGE: if set to "true", skips building container image     #
#                                                                           #
#    HOSTS_FILE: path to a file containing the hostnames of all nodes,      #
#      one per line. Content must match the output of `hostname`.           #
#                                                                           #
#    CONTAINER_LOG_DIR: path on host machine mapped to the log directory    #
#      in the container                                                     #
#                                                                           #
#    CONTAINER_HOSTS_DIR: path on host machine mapped to directory          #
#      containing hosts.txt in the container                                #
#                                                                           #
#    EXTRA_CONTAINER_VOLUMES: comma separated list of volumes to be passed  #
#      as arguments to -v option when creating the containers.              #
#                                                                           #
# ========================================================================= #

set -e
SCRIPT_DIR="$(dirname $0)"
SKIP_BUILD_IMAGE="${SKIP_BUILD_IMAGE:=false}"
HOSTS_FILE="${HOSTS_FILE:=$HOME/hosts.txt}"
CONTAINER_LOG_DIR="${CONTAINER_LOG_DIR:=$HOME/container_logs}"
CONTAINER_HOSTS_DIR="${CONTAINER_HOSTS_DIR:=$HOME/container_hosts}"

# Make sure hosts file exists
if [[ ! -f "$HOSTS_FILE" ]]; then
  echo "Hosts file '$HOSTS_FILE' not found"
  exit 1
fi

# The first node is the master
if [[ "$(hostname)" == "$(head -n 1 $HOSTS_FILE)" ]]; then
  IS_MASTER="true"
fi

# Set up all-to-all SSH access, needed by MPI
"$SCRIPT_DIR"/enable_ssh_access.py "$HOSTS_FILE"

# Install docker and build container image, this may take a while
# We first destroy old containers and images to avoid interference
"$SCRIPT_DIR"/install_docker.sh
for existing_container in $(sudo docker ps -aq); do
  sudo docker rm -f "$existing_container"
done
if [[ "$SKIP_BUILD_IMAGE" != "true" ]]; then
  for existing_image in $(sudo docker image ls -aq -f reference=virtual); do
    sudo docker rmi -f "$existing_image"
  done
  sudo docker build --no-cache -t virtual "$SCRIPT_DIR/.."
fi

# Set up overlay network, do this only on the master
if [[ -n "$IS_MASTER" ]]; then
  "$SCRIPT_DIR"/build_overlay_network.sh "$HOSTS_FILE"
fi

# Start containers attached to the network we just created
"$SCRIPT_DIR"/sync_barrier.py "$HOSTS_FILE"
mkdir -p "$CONTAINER_LOG_DIR"
mkdir -p "$CONTAINER_HOSTS_DIR"
CONTAINER_FLAGS="--network=virtual-net"
if [[ -n "$(command -v nvidia-smi)" ]]; then
  CONTAINER_FLAGS="$CONTAINER_FLAGS --runtime=nvidia"
fi
CONTAINER_FLAGS="$CONTAINER_FLAGS -v $CONTAINER_HOSTS_DIR:/root/container_hosts"
CONTAINER_FLAGS="$CONTAINER_FLAGS -v $CONTAINER_LOG_DIR:/root/dev/logs"
for volume in ${EXTRA_CONTAINER_VOLUMES//,/ }; do
  CONTAINER_FLAGS="$CONTAINER_FLAGS -v $volume"
done
CONTAINER_ID="$(sudo docker run -dit $CONTAINER_FLAGS virtual)"

# Write container host names into $CONTAINER_HOSTS_DIR/hosts.txt
"$SCRIPT_DIR"/sync_barrier.py "$HOSTS_FILE"
"$SCRIPT_DIR"/get_container_hostnames.sh "$HOSTS_FILE" "$CONTAINER_HOSTS_DIR"

# Set up all-to-all SSH access WITHIN the containers
sudo docker exec -it "$CONTAINER_ID"\
  /root/dev/tf-docker/scripts/enable_ssh_access.py\
  /root/container_hosts/hosts.txt

# Try out mpirun on master
if [[ -n "$IS_MASTER" ]]; then
  sudo docker exec -it "$CONTAINER_ID"\
    mpirun --allow-run-as-root --hostfile /root/container_hosts/hosts.txt -pernode hostname
fi

