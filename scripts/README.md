To set up the environment, simply run the following script on all nodes at the same time. This script assumes the existence of a hosts file containing the hostname of each node, one per line. The script searches for this file at `$HOME/hosts.txt` by default, but this path can be changed through the environment variable `HOSTS_FILE`. Note that the content of this file must match the output of the command `hostname`.

```
# Run on all nodes at the same time
./set_up_everything.sh
```

This script performs the following steps:

1. Enable SSH access across all nodes

2. Installs docker

3. Build the container image

4. Build an overlay network for docker containers

5. Start containers on all nodes using image from step (3)

6. Enable SSH access across all containers

7. Test whether the above steps were successful through `mpirun`

This script is idempotent. If you wish to run it again without building the container image, which takes the longest, you can simply run it with `SKIP_BUILD_IMAGE=true`. This may be useful after a restart since docker networks and containers are ephemeral. If you choose to run this script with `sudo`, you should use the `-E` flags to ensure all environment variables are actually passed to the script properly.

By default, cifar10 ships with the container under `/root/dev/dataset/cifar10`. If you wish to use other datasets inside the container, you may supply the `EXTRA_CONTAINER_VOLUMES` environment variable to map datasets on your host machine to a path inside the container. The value of this environment variable is a comma-delimited list of volumes that will each be passed to the `-v` option when creating the containers through `docker run`. For example, the following setting makes the contents of `/host/path/to/imagenet` on the host machine visible to the container under `/root/dev/dataset/imagenet`.

```
EXTRA_CONTAINER_VOLUMES="/host/path/to/imagenet:/root/dev/dataset/imagenet"
```

After setting up the environment, you can try running a basic workload using the following command:

```
# Run on one node only
# This assumes there is only one container on this node
CONTAINER_ID="$(sudo docker ps -a -q)"
sudo docker exec -it\
  -w /root/dev/models/deploy\
  -e NUM_NODES=2\
  -e NUM_GPUS=8\
  -e CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7\
  -e MODEL=resnet\
  -e DATASET=cifar10\
  -e BATCH_SIZE=128\
  -e NUM_EPOCHS=200\
  "$CONTAINER_ID"\
  ./run_distributed.sh
```

You must configure `NUM_NODES`, `NUM_GPUS` and `CUDA_VISIBLE_DEVICES` according to the particular hardware configurations of your cluster. The logs will be written to `$HOME/container_logs`, which is mapped to `/root/dev/logs` inside the container. For debugging, setting `NUM_NODES=1` and `MPI_VERBOSE=true` may be helpful. The latter prints the output of `docker exec` to the terminal in addition to the logs.

To collect logs from all nodes after running a job, simply run the following command. We run `fetch_logs.sh` in the container because the logs are owned by the root user, so running this script in the host machine will run into permission issues.

```
sudo docker exec -it -w /root/dev/tf-docker/scripts "$CONTAINER_ID" ./fetch_logs.sh /root/container_hosts/hosts.txt
```

