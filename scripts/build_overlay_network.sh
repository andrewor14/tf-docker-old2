#!/bin/bash

# ======================================================================
#  A helper script to build an overlay docker network accessible by all
#  the hosts specified in the host file. This requires the caller to
#  have root SSH access to all the other nodes in the host file.
# ======================================================================

if [[ "$#" != "1" ]] && [[ "$#" != "2" ]]; then
  echo "Usage: build_overlay_network.sh [host_file] <[network_name]>"
  exit 1
fi
HOST_FILE="$1"
NETWORK_NAME="$2"
NETWORK_NAME="${NETWORK_NAME:=virtual-net}"
SCRIPT_DIR="$(dirname $(realpath "$0"))"

# Check if host file exists
if [[ ! -f "$HOST_FILE" ]]; then
  echo "ERROR: $HOST_FILE does not exist"
  exit 1
fi

while true; do
  echo "=== Cleaning up leftover state from before ==="
  "$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "sudo docker network rm $NETWORK_NAME"
  "$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "sudo docker swarm leave -f"

  echo "=== Initializing a new swarm ==="
  INIT_RESULT="$(sudo docker swarm init)"
  JOIN_SWARM_COMMAND="$(echo "$INIT_RESULT" | grep "docker swarm join --token" | xargs)"
  JOIN_SWARM_COMMAND="sudo $JOIN_SWARM_COMMAND"
  echo "$INIT_RESULT"

  # Run the join swarm command on all other nodes
  echo "=== Asking everyone else to join our swarm ==="
  "$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "$JOIN_SWARM_COMMAND"

  # Make sure everyone is part of an active swarm
  DOCKER_NODE_LS_OUTPUT="$(sudo docker node ls)"
  SWARM_SIZE="$(echo "$DOCKER_NODE_LS_OUTPUT" | grep Active | wc -l)"
  NUM_HOSTS="$(cat "$HOST_FILE" | wc -l)"
  REMOTE_SWARM_STATUS="$("$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "sudo docker info | grep Swarm")"
  REMOTE_INACTIVE="$(echo "$REMOTE_SWARM_STATUS" | grep "inactive")"
  if [[ "$SWARM_SIZE" == "$NUM_HOSTS" ]] && [[ -z "$REMOTE_INACTIVE" ]]; then
    echo "Everyone is part of the swarm."
    break
  fi

  # Someone was not part of the swarm, so print debug message and retry
  echo "*****************************************************"
  echo "WARNING: Not everyone joined the swarm:"
  echo "SWARM_SIZE = $SWARM_SIZE"
  echo "NUM_HOSTS = $NUM_HOSTS"
  echo "$DOCKER_NODE_LS_OUTPUT"
  echo "$REMOTE_SWARM_STATUS"
  echo "... trying again in 5 seconds"
  echo "*****************************************************"
  sleep 5
done

# Build the network
sudo docker network create --driver=overlay --attachable "$NETWORK_NAME"

echo "=== Built overlay network '$NETWORK_NAME' ==="

