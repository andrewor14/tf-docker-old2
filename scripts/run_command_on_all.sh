#!/bin/bash

# ======================================================================
#  A helper script to run a command on all the hosts specified in the
#  host file. This requires the caller to have SSH access to all the
#  other nodes in the host file.
# ======================================================================

if [[ "$#" != "2" ]] && [[ "$#" != "3" ]]; then
  echo "Usage: run_command_on_all.sh [host_file] [command] <[except_this_host]>"
  exit 1
fi

# Check if host file exists
HOST_FILE="$1"
if [[ ! -f "$HOST_FILE" ]]; then
  echo "ERROR: $HOST_FILE does not exist"
  exit 1
fi

# Run the command
COMMAND="$2"
EXCEPT_THIS_HOST="$3"
while read HOST; do
  if [[ "$HOST" != "$EXCEPT_THIS_HOST" ]]; then
    ssh -n -tt -o "StrictHostKeyChecking no" "$HOST" "$COMMAND" 2>&1
  fi
done < "$HOST_FILE"

